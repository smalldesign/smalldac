#!/usr/bin/env python
#
# Modifications of code from "http://pi.gids.nl:81/adc/"

 #Required only for testing purposes
import time
import RPi.GPIO as GPIO

# Mod: converting from original BCM to BOARD
GPIO.setmode(GPIO.BOARD)

# read SPI data from MCP3208 chip, 8 possible adc's (0 thru 7)
def readadc(adcnum, clockpin, mosipin, misopin, cspin):
        if ((adcnum > 7) or (adcnum < 0)):
                return -1
        GPIO.output(cspin   , True)

        GPIO.output(clockpin, False)  # start clock low
        GPIO.output(cspin   , False)  # bring CS low

        commandout = adcnum
        commandout |= 0x18            # start bit + single-ended bit
        commandout <<= 3              # we only need to send 5 bits here
        for i in range(5):
                if (commandout & 0x80):
                        GPIO.output(mosipin, True )
                else:
                        GPIO.output(mosipin, False)
                commandout <<= 1
                GPIO.output(clockpin, True )
                GPIO.output(clockpin, False)

        adcout = 0

        # read in one empty bit, one null bit and 12 ADC bits
        # pro desetibitovy prevodnik tu bylo puvodne cislo 12
        for i in range(14):
                GPIO.output(clockpin, True )
                GPIO.output(clockpin, False)
                adcout <<= 1
                if (GPIO.input(misopin)):
                        adcout |= 0x1

        GPIO.output(cspin, True)
        
        adcout >>= 1 # first bit is 'null' so drop it
        return adcout

# change these as desired - they're the pins connected from the SPI port on the ADC to the RPi
SPICLK  = 23
SPIMISO = 21
SPIMOSI = 19
SPICS_1 = 26 # OR pin 26 for this board.
SPICS_2 = 31

# set up the SPI interface pins
GPIO.setup(SPIMOSI, GPIO.OUT)
GPIO.setup(SPIMISO, GPIO.IN )
GPIO.setup(SPICLK,  GPIO.OUT)
GPIO.setup(SPICS_1,   GPIO.OUT)
GPIO.setup(SPICS_2,   GPIO.OUT)


def read_four_20_board(SPICS):
	Varray =[0]*8
	for i in range(7):
		CODE = readadc(i, SPICLK, SPIMOSI, SPIMISO, SPICS)
		
		# NOOTE: althrough this chip measures uptill 3.3v, the
		# objective here is to interface 4-20ma through a 500ohm resistor (2-10V)
		# or 0-10 V. Further manipulation should be done as needed.
		Varray[i] = 10.0 * CODE / 4096.0 
		
		time.sleep(0.1)
	return Varray

# Only for testing, otherwise commented
while True:
	Varray=read_four_20_board(SPICS_2)
	print(Varray)
