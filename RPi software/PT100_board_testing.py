#////////////////////////
# PT100 board
#////////////////////////

# Import the ADS1x15 module for PT100 board
import Adafruit_ADS1x15

#import global_functions_variables as gfv

#------------------ RPi related ---------------------------
#Import GPIO library of the RPi
import RPi.GPIO as GPIO
# import following lib for DI and DO boards
import smbus
# Configure I2C communication
bus = smbus.SMBus(1)	# 0 = /dev/i2c-0 (port I2C0), 1 = /dev/i2c-1 (port I2C1)
# Set the GPIO pin names as per board layout instead of BCM numbering.
GPIO.setmode(GPIO.BOARD)
#GPIO.setwarnings(False)

import time


HC4051_ABC_table = [[0,0,0],[1,0,0],[0,1,0],[1,1,0],[0,0,1],[1,0,1],[0,1,1],[1,1,1]]

#R_s,address,Inh_pin,A_pin,B_pin,C_pin,Gain,Data_rate = gfv.get_config_parameters(board,'Rs','address','Inh_pin','A_pin','B_pin','C_pin','Gain','Data_rate')
address = 0x48 #0x49/0x48 
Inh_pin = 33 #7/8/33

#address = 0x49 #0x49/0x48 
#Inh_pin = 7 #7/8/33

A_pin = 10
B_pin = 11
C_pin = 12


# Create an ADS1115 ADC (16-bit) instance for PT100 board with same name as board
adc = Adafruit_ADS1x15.ADS1115(address=address, busnum=1)
GPIO.setup(Inh_pin, GPIO.OUT, initial = 1)
GPIO.setup(A_pin, GPIO.OUT, initial = 0)
GPIO.setup(B_pin, GPIO.OUT, initial = 0)
GPIO.setup(C_pin, GPIO.OUT, initial = 0)
	
	



# Rs is the reference resistor across which Iex is measured.
#R_s = 100.0


def read_PT100_board():
	# First activate the 4051s.
	GPIO.output(Inh_pin, 0)
	
	#Create an output T array to store all data.
	T_PT100_array=[0]*8
	R_array=[0]*8

	# Choose a gain of 1 for reading voltages from 0 to 4.09V.
	# Or pick a different gain to change the range of voltages that are read:
	#  - 2/3 = +/-6.144V
	#  -   1 = +/-4.096V
	#  -   2 = +/-2.048V
	#  -   4 = +/-1.024V
	#  -   8 = +/-0.512V
	#  -  16 = +/-0.256V
	# See table 3 in the ADS1015/ADS1115 datasheet for more info on gain.
	Gain = 8
	
	Data_rate = 128
	R_s = 33.0
	
	# Iterate the A, B and C pins to select each of the 8 PT100s
	for i in range(len(HC4051_ABC_table)):
		
		# Select a new PT100.
		GPIO.output(A_pin, HC4051_ABC_table[i][0])
		GPIO.output(B_pin, HC4051_ABC_table[i][1])
		GPIO.output(C_pin, HC4051_ABC_table[i][2])
		
		# Pause for half a second. This will help settle the new 
		# config and the selected PT100
		#time.sleep(0.1)
		
		k=0
		PT_avg=0
		for j in range(2):
			# Read the the differential value as per following codes
			#  - 1 = Channel 0 minus channel 3
			#  - 2 = Channel 1 minus channel 3
			#  - 3 = Channel 2 minus channel 3
			A0_A3 = adc.read_adc_difference(1, gain=Gain,data_rate=Data_rate)
			#time.sleep(1)
			A1_A3 = adc.read_adc_difference(2, gain=Gain,data_rate=Data_rate)
			#time.sleep(1)
			A2_A3 = adc.read_adc_difference(3, gain=Gain,data_rate=Data_rate)
			#time.sleep(1)
			if A2_A3!=0:
				#Compute ratiometric PT100 resistance
				R_PT100 = (2.0*A1_A3 - A2_A3 - A0_A3)/A2_A3 * R_s
				PT_avg = PT_avg + R_PT100
				k=k+1
				
		R_PT100 = PT_avg/k
		R_array[i] = round(R_PT100,2)
		print('A0_A3:',A0_A3,' A1_A3:',A1_A3,' A2_A3:',A2_A3,' R_PT100:', R_PT100)
		# Convert to T
		# Formula from http://www.mosaic-industries.com/embedded-systems/microcontroller-projects/temperature-measurement/platinum-rtd-sensors/resistance-calibration-table
		T_PT100_array[i]  = round(-244.83 + R_PT100*( 2.3419 + 0.0010664*R_PT100 ), 0)
		#if T_PT100_array[i] > 200 : T_PT100_array[i] =0
		
		time.sleep(5)
	
	# Release the inhibit pin to switch off all 4051s
	GPIO.output(Inh_pin, 1)

	#print(T_PT100_array)
	
	return R_array,T_PT100_array

	
while True:
	print(read_PT100_board())
	time.sleep(5)
