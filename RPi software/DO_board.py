#////////////////////////
# DO board interface using MCP23017
# From http://raspberrypi.link-tech.de/doku.php?id=mcp23017
#////////////////////////
import global_functions_variables as gfv



class DO_board(gfv.QObject):
	"""DO_board"""

	def __init__(self,MainWindow,instance, parent=None):
		gfv.QObject.__init__(self, parent)

		self.MW = MainWindow
		self.instance = instance

		#State the various registor names with their addresses
		self.IODIRA			 = 0x00 # Pin direction register A
		self.IODIRB			 = 0x01 # Pin direction register B
		self.OLATA			 = 0x14 # Register for outputs port A
		self.OLATB			 = 0x15 # Register for outputs port A
		self.GPIOA			 = 0x12 # Register for inputs on PORTA - only used to read status
		self.GPIOB			 = 0x13 # Register for inputs on PORTB


	#-----------------------------------------------------------------------
	# Initialization function
	#-----------------------------------------------------------------------

		self.address, = gfv.get_config_parameters('DO_board',instance,'address')
		#self.address = int(self.address,16)

		if gfv.mode=='RPi': self.init_ports()

	def	 init_ports(self):
		# Configure the device
		gfv.bus.write_byte_data(self.address, self.IODIRA , 0x00)	  # Define complete Port A as OUT-Port
		gfv.bus.write_byte_data(self.address, self.IODIRB , 0x00)	# Define complete Port B as OUT-Port:

		# Set output all output bits to 0
		gfv.bus.write_byte_data(self.address,self.OLATA,0)
		gfv.bus.write_byte_data(self.address,self.OLATB,0)

		print('Initialized DO_board: ',self.instance)

	def	 reset_ports(self):
		# Set output all output bits to 0
		gfv.bus.write_byte_data(self.address,self.OLATA,0)
		gfv.bus.write_byte_data(self.address,self.OLATB,0)

	#-----------------------------------------------------------------------
	# Define the function for connecting GUI button press to DO port
	#-----------------------------------------------------------------------
	def command_to_DO(self,ch_num,value):

		if gfv.mode == 'RPi':
			# read current status of the output pins
			A='{0:08b}'.format(gfv.bus.read_byte_data(self.address, self.GPIOA))
			B='{0:08b}'.format(gfv.bus.read_byte_data(self.address, self.GPIOB))
			#print(A,B)
		else:
			A = '00000000'
			B = '00000000'

		# Create lists of the read data. Then reverse it as smbus protocol : MSB -> LSB
		listA = list(A)
		listA.reverse()
		listB = list(B)
		listB.reverse()

		# Update the A and B according to button pressed
		if ch_num <=7:
			listA[ch_num] = str(value)
		elif ch_num >=8:
			listB[ch_num-8] = str(value) # Note because the chnum are from 0-15, whereas byte is 0-8

		# Again reverse the lists to feed back properly into SMBUS format.
		listA.reverse()
		listB.reverse()

		# Convert A and B to strings again
		A = ''.join(listA)
		B = ''.join(listB)

		# Write to DO ports
		if gfv.mode=='RPi':
			# Write the byte to MCP23017
			gfv.bus.write_byte_data(self.address, self.OLATA, int(A,2))
			gfv.bus.write_byte_data(self.address, self.OLATB, int(B,2))
		else:
			print(A,B)



	#-----------------------------------------------------------------------
	# Fucntion to update df with pin status according to times
	#-----------------------------------------------------------------------
	def get_DO(self):
		# read current status of the output pins
		A='{0:08b}'.format(gfv.bus.read_byte_data(self.address, self.GPIOA))
		B='{0:08b}'.format(gfv.bus.read_byte_data(self.address, self.GPIOB))

		# Create lists of the read data. Then reverse it as smbus protocol : MSB -> LSB
		listA = list(A)
		listA.reverse()
		listB = list(B)
		listB.reverse()
		AB = listA+listB
		print(AB)
		return(AB)



