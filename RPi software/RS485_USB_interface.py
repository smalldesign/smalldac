# RS485-USB interface

# some ref: https://www.tanzolab.it/linuxmodbus


import global_functions_variables as gfv

# import MODBUS related stuff:
from pymodbus.client.sync import ModbusSerialClient as ModbusClient
from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder

class RS485_USB_interface(gfv.QObject):
	"""EM interface class"""
	def __init__(self,MainWindow,instance,parent=None):
		gfv.QObject.__init__(self, parent)
		
		self.MW = MainWindow
		self.instance = instance
		
		self.df_local = gfv.sorted_df(gfv.df,'RS485_USB_interface',instance)
		self.units_list = list(self.df_local['ch_num'])
		print(self.units_list)
		
		# Get general board config params.
		self.port,self.timeout,self.stopbits, self.bytesize, self.parity, self.baudrate, self.method, self.register = gfv.get_config_parameters('RS485_USB_interface',instance,'port','timeout','stopbits', 'bytesize', 'parity', 'baudrate', 'method', 'register')
		self.client = ModbusClient(method=self.method, port=self.port, timeout=self.timeout, stopbits = self.stopbits, bytesize = self.bytesize,  parity=self.parity, baudrate= self.baudrate)
		self.client.connect()
		
		print("Initialized RS485_USB_interface instance ",instance)
	
	def get_32bit_float(self,unit):
		try:
			request = self.client.read_holding_registers(self.register,2,unit=unit)
			# ref: https://stackoverflow.com/questions/53010239/pymodbus-read-holding-registers
			decoder = BinaryPayloadDecoder.fromRegisters(request.registers, byteorder=Endian.Big, wordorder=Endian.Little)
			value = float('{0:.1f}'.format(decoder.decode_32bit_float()))/1000.0
		except:
			value = -1
			print("MODBUS error");
		#print('EM value:',value)
		return value
		