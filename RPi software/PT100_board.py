#////////////////////////
# PT100 board
#////////////////////////

# Import the ADS1x15 module for PT100 board
import Adafruit_ADS1x15

import global_functions_variables as gfv


class PT100_board(gfv.QObject):
	"""PT100_board"""
	
	#PT100_update = gfv.pyqtSignal(list, name='PT100_update')

	def __init__(self,MainWindow,instance, parent=None):
		gfv.QObject.__init__(self, parent)
		
		self.MW = MainWindow
		self.instance = instance
		
		self.address,self.R_s,self.Inh_pin,self.A_pin,self.B_pin,self.C_pin,self.Gain,self.Data_rate = gfv.get_config_parameters('PT100_board',instance,'address','R_s','Inh_pin','A_pin','B_pin','C_pin','Gain','Data_rate')
		#self.address = int(self.address,16)
		
		if gfv.mode=='RPi':
			try:
				# Create an ADS1115 ADC (16-bit) instance for PT100 board with same name as board
				self.ADC = Adafruit_ADS1x15.ADS1115(address=self.address, busnum=1)
				gfv.GPIO.setup(self.Inh_pin, gfv.GPIO.OUT, initial = 1)
				gfv.GPIO.setup(self.A_pin, gfv.GPIO.OUT, initial = 0)
				gfv.GPIO.setup(self.B_pin, gfv.GPIO.OUT, initial = 0)
				gfv.GPIO.setup(self.C_pin, gfv.GPIO.OUT, initial = 0)
			except:	 
				gfv.traceback.print_exc()
				gfv.GPIO.cleanup()
				gfv.sys.exit()
		print('Initialized PT100_board instance:',self.instance)


	def read_PT100_board(self):
		# First activate the 4051s.
		gfv.GPIO.output(self.Inh_pin, 0)
		
		#Create an output T array to store all data.
		T_PT100_array=[0]*8

		# Choose a gain of 1 for reading voltages from 0 to 4.09V.
		# Or pick a different gain to change the range of voltages that are read:
		#  - 2/3 = +/-6.144V
		#  -   1 = +/-4.096V
		#  -   2 = +/-2.048V
		#  -   4 = +/-1.024V
		#  -   8 = +/-0.512V
		#  -  16 = +/-0.256V
		# See table 3 in the ADS1015/ADS1115 datasheet for more info on gain.
		
		# Iterate the A, B and C pins to select each of the 8 PT100s
		for i in range(len(gfv.HC4051_ABC_table)):
			
			# Select a new PT100.
			gfv.GPIO.output(self.A_pin, gfv.HC4051_ABC_table[i][0])
			gfv.GPIO.output(self.B_pin, gfv.HC4051_ABC_table[i][1])
			gfv.GPIO.output(self.C_pin, gfv.HC4051_ABC_table[i][2])
			
			# Pause for half a second. This will help settle the new 
			# config and the selected PT100
			gfv.time.sleep(0.1)
			
			k=0
			PT_avg=0
			while k<=5:
				# Read the the differential value as per following codes
				#  - 1 = Channel 0 minus channel 3
				#  - 2 = Channel 1 minus channel 3
				#  - 3 = Channel 2 minus channel 3
				A0_A3 = self.ADC.read_adc_difference(1, gain=self.Gain , data_rate=self.Data_rate)
				A1_A3 = self.ADC.read_adc_difference(2, gain=self.Gain , data_rate=self.Data_rate)
				A2_A3 = self.ADC.read_adc_difference(3, gain=self.Gain , data_rate=self.Data_rate)
				
				if A2_A3!=0:
					#Compute ratiometric PT100 resistance
					R_PT100 = (2.0*A1_A3 - A2_A3 - A0_A3)/A2_A3 * self.R_s
					PT_avg = PT_avg + R_PT100
					k=k+1
			
			#print('A0_A3:',A0_A3,' A1_A3:',A1_A3,' A2_A3:',A0_A3)
			R_PT100 = PT_avg/k
			#print(R_PT100)
			# Convert to T
			# Formula from http://www.mosaic-industries.com/embedded-systems/microcontroller-projects/temperature-measurement/platinum-rtd-sensors/resistance-calibration-table
			T_PT100_array[i]  = round(-244.83 + R_PT100*( 2.3419 + 0.0010664*R_PT100 ), 0)

			if T_PT100_array[i] > 200 or T_PT100_array[i] < 0 : T_PT100_array[i] = 0
			
		
		# Release the inhibit pin to switch off all 4051s
		gfv.GPIO.output(self.Inh_pin, 1)
		

		#self.PT100_update.emit(T_PT100_array)

		#print("PT100,",self.instance,",",T_PT100_array)
		
		return T_PT100_array
