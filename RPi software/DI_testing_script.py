#////////////////////////
# DI board interface using MCP23017
# From http://raspberrypi.link-tech.de/doku.php?id=mcp23017
#////////////////////////


# Good for testing DI board individually!!!

#-----------------------------------------------------------------------
# On the RPi side
#-----------------------------------------------------------------------
import smbus
import RPi.GPIO as GPIO
GPIO.setmode(GPIO.BOARD)
import time


# Configure I2C communication
bus = smbus.SMBus(1)    # 0 = /dev/i2c-0 (port I2C0), 1 = /dev/i2c-1 (port I2C1)


# setup pin 18 amd 22 as INTA and INTB
INTA_pin = 18
INTB_pin = 22

# set as inputs
GPIO.setup(INTA_pin,GPIO.IN)
GPIO.setup(INTB_pin,GPIO.IN)
        

#-----------------------------------------------------------------------
# On the MCP23017 side
#-----------------------------------------------------------------------
#State the various registor names with their addresses
DEVICE_ADDRESS 	= 0x27 # Device address (A0-A2). Verify through "sudo i2cdetect -y 1"
IODIRA 			= 0x00 # Pin direction register A
IODIRB 			= 0x01 # Pin direction register B
GPIOA  			= 0x12 # Register for inputs on PORTA
GPIOB  			= 0x13 # Register for inputs on PORTB
GPINTENA		= 0x04 # GP interrupt for port A
GPINTENB		= 0x05 # GP interrupt for port B
IOCONA			= 0x0a # INT control for A
IOCONB			= 0x0b # INT control for port B
INTFA			= 0x0E # interrupt flag for port A
INTFB			= 0x0F # interrupt flag for port B
INTCAPA			= 0x10 # GP interrupt for port A
INTCAPB			= 0x11 # GP interrupt for port B

# Configure the device
bus.write_byte_data(DEVICE_ADDRESS, IODIRA , 0xFF) 	# Define complete Port A as IN-Port
bus.write_byte_data(DEVICE_ADDRESS, IODIRB , 0xFF)	# Define complete Port B as IN-Port:

# Activate Interrupt OnChange for all pins on Port A abd B. 0xFF = 1111 1111
bus.write_byte_data(DEVICE_ADDRESS, GPINTENA, 255)		
bus.write_byte_data(DEVICE_ADDRESS, GPINTENB, 129)

# Connect Interrupt-Pin A with the one of B (MIRROR = 1, 0x40)
bus.write_byte_data(DEVICE_ADDRESS, IOCONA, 0x00)
bus.write_byte_data(DEVICE_ADDRESS, IOCONB, 0x00)

#Read current status of the MCP23017 GPIOs to flush and reset the interrupts.
A ='{0:08b}'.format(bus.read_byte_data(DEVICE_ADDRESS, GPIOA))
B ='{0:08b}'.format(bus.read_byte_data(DEVICE_ADDRESS, GPIOB))
print('GPIO',A,B)

# Define the callback function.
# more elaboration needed as per Kivy reqs.
def DI_INTA_callback(channel):
	#global A,B
	print('INTA')
	#Read INTF to identify which pin caused interrupt
	A_INTF='{0:b}'.format(bus.read_byte_data(DEVICE_ADDRESS, INTFA))
	B_INTF='{0:b}'.format(bus.read_byte_data(DEVICE_ADDRESS, INTFB))
	
	# Print the A or B port number that changed
	print('INTFA:'+A_INTF + ';' + 'INTFB:'+B_INTF)
	
	if int(A_INTF) > 0: print('A',len(str(A_INTF)) )
	elif int(B_INTF) > 0: print('B',len(str(B_INTF)) )

	# Clear the interrupts for next cycle.
	A='{0:08b}'.format(bus.read_byte_data(DEVICE_ADDRESS, GPIOA))
	B='{0:08b}'.format(bus.read_byte_data(DEVICE_ADDRESS, GPIOB))
	print('GPIO',A,B)
	
def DI_INTB_callback(channel):
	#global A,B
	print('INTB')
	#Read INTF to identify which pin caused interrupt
	A_INTF='{0:b}'.format(bus.read_byte_data(DEVICE_ADDRESS, INTFA))
	B_INTF='{0:b}'.format(bus.read_byte_data(DEVICE_ADDRESS, INTFB))
	
	# Print the A or B port number that changed
	print('INTFA:'+A_INTF + ';' + 'INTFB:'+B_INTF)
	
	if int(A_INTF) > 0: print('A',len(str(A_INTF)) )
	elif int(B_INTF) > 0: print('B',len(str(B_INTF)) )

	# Clear the interrupts for next cycle.
	A='{0:08b}'.format(bus.read_byte_data(DEVICE_ADDRESS, GPIOA))
	B='{0:08b}'.format(bus.read_byte_data(DEVICE_ADDRESS, GPIOB))
	print('GPIO',A,B)
	

# Enable interrupts on INTA_pin or B 
# Ref: http://raspberrywebserver.com/gpio/using-interrupt-driven-gpio.html
# Also ref: https://medium.com/@rxseger/interrupt-driven-i-o-on-raspberry-pi-3-with-leds-and-pushbuttons-rising-falling-edge-detection-36c14e640fef
# NOTE: do not use "bouncetime" here as it will cause the interrupts on MCP23017 to remain unusable
# Also note, due to error in board, INTA and INTB must be interchanged.
GPIO.add_event_detect(INTB_pin, GPIO.FALLING, callback=DI_INTA_callback)

GPIO.add_event_detect(INTA_pin, GPIO.FALLING, callback=DI_INTB_callback)


# Incase there is another or n number of additional DI boards, specific
# add_event_detects for each board must be used and corresponding 
# callback functions need to be made.

 #Use the following to test this code.
i=0
try:
	while True:
		i=i+1
		print(i,"waiting...")
		time.sleep(0.5)
except KeyboardInterrupt:
	GPIO.cleanup()

