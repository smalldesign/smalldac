# Updated on 19 Dec 2018

#------------------ PyQT5 related libs ---------------------------
# Ref: from https://stackoverflow.com/questions/43126721/pyqt-detect-resizing-in-widget-window-resized-signal
# Concept: Instead of using code directly from QTDesigner, insert an main class such as below that calls other 
# classes, such as the TabWidget. This main class has the resizeEvent alert system that broadcasts resize action.  
from PyQt5 import QtCore, QtGui, QtWidgets
from PyQt5.QtCore import QObject


# This is necessary to announce to all GUI that something happned from user side.
# need more understanding/elaborate comments here..
from PyQt5.QtCore import pyqtSignal

# Make the continiously running GUI part threaded so its non-blocking..
# need more understanding/elaborate comments here..
from PyQt5.QtCore import QThread



#------------------ plotting related ---------------------------
# Note All this is now based on pyqtgraph.
import pyqtgraph as pg

#----------------------- RS-485 Modbus Power meter
# Check installed USB devices by:
# python -m serial.tools.list_ports -v
# some ref: https://www.tanzolab.it/linuxmodbus
#from pymodbus.client.sync import ModbusSerialClient as ModbusClient


#------------------ Misc ---------------------------
from functools import partial
# For cases where testing is going on, we need random numbers to simulate IO cards.
import random

# Pandas provide the ability to read and write csv files very easily.
# also allowing the use of 'dataframes'.
import pandas as pd
# Set a pandas option to detect empty cells.
# Ref: https://pandas.pydata.org/pandas-docs/stable/generated/pandas.Series.isnull.html
# "Characters such as empty strings '' or numpy.inf are not considered NA values (unless you set pandas.options.mode.use_inf_as_na = True)."
# pd.set_option('mode.use_inf_as_na',True)

# to interface time related things, like system time, time conversions, etc.
import time
from datetime import datetime, timedelta
# time related lib.
from calendar import timegm
# Common lib for mathematical computations in python.
import numpy as np
# Lib for interfacing system level things.
import sys
import os
# Lib for stats support to computing.
import statistics
# Lib for parsing string as python command.
import ast
# Lib for interfacing errors.
import traceback




#------------------ RPi related ---------------------------
#Import GPIO library of the RPi
import RPi.GPIO as GPIO
# import following lib for DI and DO boards
import smbus
# Configure I2C communication
bus = smbus.SMBus(1)	# 0 = /dev/i2c-0 (port I2C0), 1 = /dev/i2c-1 (port I2C1)
# Set the GPIO pin names as per board layout instead of BCM numbering.
GPIO.setmode(GPIO.BOARD)
#GPIO.setwarnings(False)


import re



#------------------ IO boards related---------------------------
import DI_board
import DO_board
import four_20_board 
import PT100_board
#import TC_board

# for finding the tyep of dataframe cell
import types

#------------------# Import the frontend.py---------------------------
# 'frontend.py' is the screen GUI created in QT Designer as frontend.ui.
# Import the gui widget named in it.
# Note that the original frontend is modified by adding following to pyqtgraph's PlotWidget
# 'axisItems={'bottom': xtime.TimeAxisItem(orientation='bottom')}'
# also add 'import xtime' to the bottom of frontend.py.
# also make resources_rc_rc as resources_rc
import frontend 


import re

#------------------ Initialize global varaibles used all across this program---------------------------
def init_global_variables():
	# For letting know that if this code is being on a RPi-less testing or interacting to an RPi
	global mode 
	#mode = "testing"
	mode = "RPi"
	
	# Dictionary for holding all the IO boards related configuration values
	# NOTE: this method is prefferable to using the original dataframe as the dataframe
	# will be modified by adding values for each time, and thus will be come more and more heavy to call every now and then
	global dict_IO
	dict_IO = {}
	
	# the dataframe that holds all board and pin configurations
	global df
	df = pd.read_excel('Setup.xls', sheetname='IO',encoding='latin-1', comment='#')
	
	global df_boardconfig
	df_boardconfig = pd.read_excel('Setup.xls', sheetname='Boards_config',encoding='latin-1', comment='#')
	
	global df_algorithm
	df_algorithm = pd.read_excel('Setup.xls', sheetname='Algorithm',encoding='latin-1', comment='#')

	# declare the DI variables as globals so that interrupts can directly update them.
	global_vars = df['variable'].values.tolist()
	for var in global_vars:
		globals()[var] = 0
		
	# Get header list
	global	column_headers
	column_headers = ['Date','Time']
	column_headers.extend(df['variable'])
	
	# Establish the HC4051 A, B,C pins selection table used for PT100 and TC boards
	global HC4051_ABC_table
	HC4051_ABC_table = [[0,0,0],[1,0,0],[0,1,0],[1,1,0],[0,0,1],[1,0,1],[0,1,1],[1,1,1]]
	

	# Configure I2C communication
	bus = smbus.SMBus(1)	# 0 = /dev/i2c-0 (port I2C0), 1 = /dev/i2c-1 (port I2C1)
	
	# PLots color list
	global colorlist,markerlist
	colorlist =['b','g','r','c','m','y','k']
	markerlist = ['o','v','^','<','>','*','p','s','h','+','x']
	
	global firstplot
	firstplot = True
	
	global Auto_mode_status
	Auto_mode_status = 0
	
	

	

#------------------ Function to readup config parameters of each board and port ---------------------------
def get_config_parameters(board,instance,*args):
	df2 = sorted_df(df_boardconfig,board,instance)
	#print(df2)
	param_name=[]
	dict_IO[board]={}
	for param in args:
		#print(param)
		val= df2.loc[df2['Config_param']== param,'Config_value'].values[0]
		#print(param,val)
		if (str(val)[0] == '0') and (str(val)[1] =='x'):
			val = int(val,16)
		else:
			val = int(val)
		param_name.append(val)
	return tuple(param_name)
   

def sorted_df(df,board,instance):
	df = df.loc[df['board'] == board,:]
	df = df.loc[df['instance'] == instance,:]
	return df


#------------------ This the GUI handling mother function.---------------------------
# Updated from ref: https://wiki.python.org/moin/PyQt/Threading%2C_Signals_and_Slots
class MainWindow(QtWidgets.QTabWidget, frontend.Ui_TabWidget1):
	# declare a signal/slot system that will be triggred is window is resized
	resized = QtCore.pyqtSignal()
	
	# Init, where all things begin as roots
	def	 __init__(self):#, parent=None):
		#Set PlotWidget options before loading widget
		pg.setConfigOption('background', 'w') 
		
		# -----------Initiate the UI-----------
		# ref: https://nikolak.com/pyqt-qt-designer-getting-started/
		# On the use of 'super': "Simple reason why we use it here is that it allows us to
		# access variables, methods etc in the design.py* file". 
		# *In our case frontend.py.
		super(self.__class__, self).__init__()
		
		# This is defined in frontend.py file on creation.
		# It sets up layout and widgets that are defined through QT designer.
		self.setupUi(self)	
		
		#--------------Make GUI scaled and sized as per window (Very important)--------------------
		# First get the relative positions of widgets wrt original screen as configured in QTDesigner
		relative_widget_positions,widgetlist = self.get_relative_widget_positions()
		#Then connect the resized event with a function that manipulates the widgets
		self.resized.connect(lambda: self.reposition_widgets_on_resize(relative_widget_positions,widgetlist))
		
		# Initialize the global variables, available throughtou tht program.
		init_global_variables()
		
		# setup plot windows
		self.setup_plots()

		#--------------------------initiate the IO boards-------------------------------
		if mode =='RPi':
		  self.init_all_boards()
		
		# Update DI from DI boards
		self.DI_1.DI_update.connect(self.update_DI_on_interrupt)
		
		# Init the buttons to be connected to the DO board
		for pb in self.findChildren((QtWidgets.QPushButton)):
			pb.clicked.connect(self.btn_pressed)
		
		# get object references of algorithm and stop buttons that will be affected by algorithm mode
		self.unaffected_buttons=['Auto_mode','STOP','Record_brix_and_ph_value']
		self.affected_buttons  = self.findChildren((QtWidgets.QPushButton))
		for pb_name in self.unaffected_buttons:
			pb_ref = self.findChild((QtWidgets.QPushButton),pb_name)
			if pb_ref: self.affected_buttons.remove(pb_ref)
		
		# From ref: "We create a single Worker instance that we can reuse as required."
		# This worker is meant to do all the IO and disc read/write operations.
		self.thread = Worker(self)
		
		

		self.start_plotting=False
		
		self.new_log_file=True
		
		# update filename of the log file.
		self.thread.filename_signal.connect(self.update_filename)
		
		
		# Adapting From Ref: " The custom output string 'IO_data_ready' signal is connected to the 
		# "self.update_GUI" slot so that we can update the .... every time a new ..."
		self.thread.IO_data_ready.connect(self.update_GUI)
	
		self.thread.algorithm_message_signal.connect(self.update_message_window)
		# signal to act on
		self.thread.algorithm_action_signal.connect(self.algorithm_DO_action)
		

		# Plot only when sceen changed to plot screen.
		#self.currentChanged.connect(self.do_all_plots)
		#self.thread.Plot_data.connect(self.do_plots)

		try:
			self.thread.start()
		except:
			self.quit_on_error()
			
		

	
	#-----------------------------------------------------------------
	# Find the various children widgets of Overviewtab and size them up as per original dimensions.
	def get_relative_widget_positions(self):
		# get list of widgets along with their positions, sizes
		widgetlist = self.findChildren((QtWidgets.QLCDNumber , QtWidgets.QPushButton, QtWidgets.QLabel, QtWidgets.QProgressBar, QtWidgets.QComboBox, QtWidgets.QPlainTextEdit, QtWidgets.QFrame))
		# Compute original frame geometry
		g = self.geometry()
		g_x = g.x()
		g_y = g.y() 
		g_w = g.width()
		g_h = g.height()
		# initate the relative positions array
		relative_widget_positions =[]
		# iterate over each widget to compute its relative position and size wrt original frame height and width
		for w in widgetlist:
			pos = w.pos()  # Note pos() gives position relative to parent's geometry and not absolute screen geometry.
			rel_x = pos.x()/g_w
			rel_y = pos.y()/g_h
			rel_w = w.width()/g_w
			rel_h = w.height()/g_h
			relative_widget_positions.append([rel_x,rel_y,rel_w,rel_h])
		return relative_widget_positions,widgetlist
	
	#-----------------------------------------------------------------	
	# Trigger this when window is resized... Need to better this understanding/comment.
	def resizeEvent(self, event):
		self.resized.emit()
		return super(MainWindow, self).resizeEvent(event)

	#-----------------------------------------------------------------
	# Name is self-evident - reposition and resize the widgets after mainwindow is resized.
	def reposition_widgets_on_resize(self,relative_widget_positions,widgetlist):
		# Compute current frame geometry
		g = self.geometry()
		g_x = g.x()
		g_y = g.y() 
		g_w = g.width()
		g_h = g.height()
		# Reposition and resize as per current frame.
		for w,r in zip(widgetlist,relative_widget_positions):
		  w.resize(r[2]*g_w , r[3]*g_h)
		  w.move(r[0]*g_w , r[1]*g_h)
			
	#-----------Setup the plots---------------------
	def setup_plots(self):
		global df
		# Modify plots to the pre-set tabs for this purpose.
		plots = self.findChildren((pg.PlotWidget))
		for plt in plots:
			plt.plotItem.showGrid(True, True, 0.7)
			plt.setLabel('left', 'Temperature', units='C')
			plt.setLabel('bottom', 'Time', units="hh:mm")
			plt.enableAutoRange()
			
			#--------legend-------------
			plt.addLegend()
			# add custom legend item, else the legned keeps on expanding as everytime plots are made
			# create a list of plot variables for this particular plot
			Ys = df.loc[df['plot_specs']==plt.objectName(),'variable'].values.tolist()
			# create listof pens
			globals()['pens']=['b', 'g', 'r', 'c', 'm', 'y', 'k', 'w']			
			for Y in Ys:
				# ref: https://stackoverflow.com/questions/16698074/pyqtgraph-add-legend-for-lines-in-a-plot
				l = pg.PlotDataItem(pen=pens[Ys.index(Y)])
				plt.plotItem.legend.addItem(l,Y)
	
				
	#-----------Init all the IO boards---------------------			 
	def init_all_boards(self):
		try:
			self.DI_1 = DI_board.DI_board(self,1)
			self.DO_1 = DO_board.DO_board(self,1)
			self.PT100_1 = PT100_board.PT100_board(self,1)
			self.PT100_2 = PT100_board.PT100_board(self,2)
			self.four_20_board_1 = four_20_board.four_20_board(self,1)
		except:
			self.quit_on_error()

	#------------------ Function to act on on-screen button presses---------------------------
	def btn_pressed(self):
		# identify what was pressed.
		btn = self.sender()
		# get its object name
		btn_objectName = btn.objectName() 
		# find if this button was pressed or released. Then create a new variable that carries its status.
		if btn.isChecked(): value = 1
		else: value = 0
		
		# If the button pressed is 'Stop', the stop all program with GUI. Also make a clean exit - 
		# set GPIOs to inputs. This is necessary for resetting any that were, else even if program is shutdown
		# GPIOs remain in their state of on. Also making sure that next time the program starts, the GPIOs are setup again.
		if btn_objectName == 'STOP' and value==1:
			self.DO_1.reset_ports()
			print('Stopping system...')
			GPIO.cleanup()
			sys.exit()
	
		if btn_objectName == 'new_logfile':
			self.new_log_file = True
			return
		
		# Code for combobox type of inputs.
		if btn_objectName == 'Record_Brix_and_pH_value':
			for cb in self.findChildren((QtWidgets.QComboBox)):
				#print(cb.objectName())
				globals()[cb.objectName()] = str(cb.currentText())
				return
			

		# Act on the DO ports after DO button press, only if Algorithm_mode is off and physical key is in auto position..
		if globals()['PLC_mode_DI'] == 1:
			# Button for toggling Auto or Screen_manual modes
			if btn_objectName == 'Auto_mode':
				
				# Also get the indicator for Algorithm_mode
				Auto_mode_indicator_ref = self.findChild((QtWidgets.QLabel),'Auto_mode_IND')
				# Check Algorithm_mode's values and act accordinly
				if value == 1:
					# update the global variable. 
					globals()['Auto_mode_status'] = 1
					# switch on the LED on screen
					Auto_mode_indicator_ref.setPixmap(QtGui.QPixmap(":/images/LED_ON"))
					# now get all buttons other than "Algorithm_mode" and "STOP" and disable them. 
					# for pb in self.affected_buttons:
						# pb.setEnabled(False)
				else:
					# reset all ports to zero. This will stop all - Very Necessary!!!
					self.DO_1.reset_ports()
					globals()['Auto_mode_status'] = 0
					Auto_mode_indicator_ref.setPixmap(QtGui.QPixmap(":/images/LED_OFF"))
					
					for pb in  self.affected_buttons:
						pb.setEnabled(True)
				
			# else operate the buttons normally
			else:
				#Get specific channel number and board name according to button pressed
				ch_num = df.loc[df['variable'] == btn_objectName,'ch_num'].values[0]
				board = df.loc[df['variable'] == btn_objectName,'board'].values[0]
				# Output the button action to DO board
				if mode == 'RPi':
					self.DO_1.command_to_DO(int(ch_num),str(value))
					# update onscreen button indicators as DI feedback is not available in current implementation.
					button_indicator_DI = self.findChild((QtWidgets.QLabel),btn_objectName+'_DI')
					button_indicator_IND = self.findChild((QtWidgets.QLabel),btn_objectName+'_IND')
					if button_indicator_DI:
						if value==1 : button_indicator_DI.setPixmap(QtGui.QPixmap(":/images/LED_ON"))
						else: button_indicator_DI.setPixmap(QtGui.QPixmap(":/images/LED_OFF"))
					elif button_indicator_IND:
						if value==1 : button_indicator_IND.setPixmap(QtGui.QPixmap(":/images/LED_ON"))
						else: button_indicator_IND.setPixmap(QtGui.QPixmap(":/images/LED_OFF"))
			
			

					
	# Also get inputs defined on screen by user
	def ComboBox_selected(self,cb):
		userin_vars = [var for var in df.loc[df['board']=='user_input','variable'].values]
		for var in userin_vars:
		  var = self.findChild((QtWidgets.QComboBox),str(var))
		  data_row.append(var.currentText())
		return data_row

			  
	# Update all 'LCDwidgets' & LEDs on screen with acquired data
	def update_GUI(self,data_row):
		#print(data_row)
		for variable in column_headers:
			value = data_row[column_headers.index(variable)]
			globals()[variable] = value
			#print("Variable:",variable,value)
			
			# find that particular variable amongst the GUI and of type LCD number
			var = self.findChild((QtWidgets.QLCDNumber),str(variable))
			if var:
				#print(column_headers.index(variable),'LCD:',var.objectName(),value)
				# get value of that particular variable in current data_row
				var.display(value) 
				continue
			# Also update the LED indicators. Currently not all are connected to 
			var = self.findChild((QtWidgets.QPushButton),str(variable))
			if var:
				var_IND = self.findChild((QtWidgets.QLabel),str(variable)+'_IND')
				var_DI = self.findChild((QtWidgets.QLabel),str(variable))
				if var_IND:
					#print(column_headers.index(variable),'QLabel IND:',var.objectName(),var_IND.objectName(),value)
					# get value of that particular variable in current data_row
					if value == 1: 
						var_IND.setPixmap(QtGui.QPixmap(":/images/LED_ON"))
					else: 
						var_IND.setPixmap(QtGui.QPixmap(":/images/LED_OFF"))
					#continue
				elif var_DI:
					#var_ind = self.findChild((QtWidgets.QLabel),str(variable))
					#print(column_headers.index(variable),'QLabel_DI:',var.objectName(),value)
					# get value of that particular variable in current data_row
					if value==1: 
						var_DI.setPixmap(QtGui.QPixmap(":/images/LED_ON"))
					else: 
						var_DI.setPixmap(QtGui.QPixmap(":/images/LED_OFF"))		
	
	
	
	def update_message_window(self,message):
		messagewindow = self.findChildren((QtWidgets.QPlainTextEdit))
		#messagewindow.clear()
		for mw in messagewindow:
			mw.appendPlainText(message)
	
	
	def algorithm_DO_action(self,ch_name,value):
		try:
			#Get specific channel number and board name according to action command
			ch_num = int(df.loc[df['variable'] == ch_name,'ch_num'].values[0])
			
			#board = df.loc[df['variable'] == btn_objectName,'board'].values[0]
			# Command the DO pin to go high or low.
			self.DO_1.command_to_DO(ch_num,str(value))
			
			if ch_name in self.unaffected_buttons:
				btn_ref = self.findChild((QtWidgets.QPushButton),ch_name)
				btn_ref.setChecked(False)
				
			# find LED on display corresponding to the DO pin
			DO_DI = self.findChild((QtWidgets.QLabel),ch_name+'_DI')
			DO_IND = self.findChild((QtWidgets.QLabel),ch_name+'_IND')
			# check if it exists
			if DO_IND: DO_indicator = DO_IND
			if DO_DI: DO_indicator = DO_DI 
			
			# On or Off it based on value.
			if value==1: 
				DO_indicator.setPixmap(QtGui.QPixmap(":/images/LED_ON"))
			else: 
				DO_indicator.setPixmap(QtGui.QPixmap(":/images/LED_OFF"))
		except:
			self.quit_on_error()


			

	# Do all the plotting on the plot specific tabs
	def do_plots(self,df_sample):
		# Make a list of last two times. This hack helps create line between plotted points
		# First create a list of date+time together.
		Xdata_datetime = [a+','+ b for a,b in zip( df_sample['Date'].tolist(),df_sample['Time'].tolist() ) ]
		# Convert into 'dateimtime'object
		Xdata_datetime = [datetime.strptime(var, '%d/%m/%y,%H:%M:%S') for var in Xdata_datetime]
		# then a list of dates converted into datetime object	   
		Xdata_date = [datetime.strptime(var, '%d/%m/%y') for var in df_sample['Date'].tolist()]
		# subtract date+time - date to get the time in tersm of seconds from midnight.
		Xdata = [ (xdatetime - xdate).total_seconds() for xdatetime,xdate in zip(Xdata_datetime,Xdata_date)]
		# Get a list of plots from the frontend.py
		plots = self.findChildren((pg.PlotWidget))
		# For each plot....
		for plt in plots:
			# Get the variables that are maked to be plot in this particular plot
			Ys = df.loc[df['plot_specs']==plt.objectName(),'variable'].values.tolist()
			#print(plt.objectName(),Ys)
			#curve = plt.getPlotItem().plot()
			for Y in Ys:
				# Get the Y data points for last two times
				Ydata = [int(var) for var in df_sample[Y].tolist()]
				# do the plots
				#curve =  pg.PlotCurveItem()
				plt.plot(Xdata,Ydata,pen=pens[Ys.index(Y)])	 
		#pg.QtGui.QApplication.processEvents()

		
	def update_filename(self, value):
		self.filename=value
		self.start_plotting=True
		logname_indicator = self.findChild((QtWidgets.QLineEdit),'current_logfile_name')
		logname_indicator.setText(self.filename)
		
		
	# Do all the plotting on the plot specific tabs
	def do_all_plots(self,i):
		if self.start_plotting==True:
			with open(self.filename,'r') as f:
				df_local = pd.read_csv(f)
			
			tablist = self.findChildren((QtWidgets.QTabWidget))
			print(i,tablist)
			#tab=tablist[i]
			tab = QtWidgets.QTabWidget.currentWidget(self)
			print(tab.objectName)
			plots = tab.findChildren((pg.PlotWidget))
			if len(plots)!=0:
				Xdata = [datetime.strptime(var, '%H:%M:%S') for var in df_local['Time'].tolist()]
				Xdata = [(var-Xdata[0]).total_seconds() for var in Xdata]
				for plt in plots:
					Ys = df.loc[df['plot_specs']==plt.objectName(),'variable'].values.tolist()
					for Y in Ys:
						Ydata = [int(var) for var in df_local[Y].tolist()]
						plt.plot(Xdata,Ydata,pen=pens[Ys.index(Y)]) 
						
	
	def update_DI_on_interrupt(self,DI_name,value):
		# update the DI variable
		globals()[DI_name] = value
		# update its indicator on screen
		status_indicator = self.findChild((QtWidgets.QLabel),DI_name)
		if status_indicator:
			if value == 1:
				status_indicator.setPixmap(QtGui.QPixmap(":/images/LED_ON"))
			else :
				status_indicator.setPixmap(QtGui.QPixmap(":/images/LED_OFF"))
	
	def quit_on_error(self):
		self.DO_1.reset_ports()
		traceback.print_exc()
		GPIO.cleanup()
		sys.exit()
	
	
	
#------------------ The worker loop that reads IOs and acts on algorithm.---------------------------   
# ref: https://wiki.python.org/moin/PyQt/Threading%2C_Signals_and_Slots 
# "The worker thread is implemented as a PyQt thread rather than a Python thread since we want to 
# take advantage of the signals and slots mechanism to communicate with the main application"
class Worker(QThread):

	# This defines a signal called 'rangeChanged' that takes two
	# integer arguments.
	IO_data_ready = pyqtSignal(list, name='IO_data_ready')
	
	#Plot_data = pyqtSignal(object, name='Plot_data')
	
	filename_signal = pyqtSignal(str, name='filename_signal')
	
	# Signals to send algorithm conclusions and commands to main window.
	#algorithm_message_signal = pyqtSignal(str, name='algorithm_message_signal')
	algorithm_action_signal = pyqtSignal(str, int, name='algorithm_action_signal')
	
	algorithm_message_signal = pyqtSignal(str, name='algorithm_message_signal')
	
	#DO_signal = pyqtSignal(str,int, name='DO_signal')
	
	def __init__(self,MainWindow,parent= None):
		QThread.__init__(self, parent)
		self.MW = MainWindow
		self.exiting = False
		print('Worker init complete')

	# from ref: "Before a Worker object is destroyed, we need to ensure that it stops processing. For this reason, 
	# we implement the following method in a way that indicates to the part of the object that performs the 
	# processing that it must stop, and waits until it does so."
	def __del__(self):
	  self.exiting = True
	  self.wait()
	
	def stop(self):
		self.quit()
		self.exit()

	# Get all inputs from all boards.
	def update_IO(self,data_row):
		if mode== 'RPi':
		# Extend the data_row one card by one card
			data_row.extend(self.MW.PT100_1.read_PT100_board())
			data_row.extend(self.MW.PT100_2.read_PT100_board())
			mA420_array = self.MW.four_20_board_1.read_4_20mA_board()
			data_row.extend(self.compute_derived_params(mA420_array))
			data_row.extend(self.MW.DI_1.get_DI())
			data_row.extend(self.MW.DO_1.get_DO())
			for var in df.loc[df['board'] == 'user_input','variable'].values:
				 data_row.append(globals()[var])
			#request1 = client1.read_holding_registers(100,2,unit=0x02)

		else:
			data_row.extend(random.sample(range(1, 200), 8))
			data_row.extend(random.sample(range(1, 200), 8))
			data_row.extend(random.sample(range(1, 100), 8))
			data_row.extend(random.sample(range(1, 800), 4))
			# ref: https://stackoverflow.com/questions/6824681/get-a-random-boolean-in-python
			data_row.extend(list(np.random.randint(2, size=16)))
			data_row.extend(list(np.random.randint(2, size=16)))
			for var in df.loc[df['board'] == 'user_input','variable'].values:
				data_row.append(globals()[var])
		return data_row
	
	def compute_derived_params(self, mA420_array):
		a = []
		#print(mA420_array)
		for value in mA420_array:
			cal = int(1200.0/8.0*(value-2.0))
			#print(cal)
			a.append(cal)
		#a[4] =	 self.maprange([2,10],[4,14],mA420_array[4])
		a[4] = 14.0/8.0*(mA420_array[4]-2.0)
		#print(a)
		return a
	
	# ref:	https://rosettacode.org/wiki/Map_range#Python
	def maprange(self,a, b, s):
		(a1, a2), (b1, b2) = a, b
		return	b1 + ((s - a1) * (b2 - b1) / (a2 - a1))
		
		
	def make_new_logfile(self,localtime,df_data):
		self.MW.DI_1.reset_counters()
		# Creatre file for logging
		localtime_str = localtime.strftime('%Y_%m_%d_%H_%M')
		filename = 'logfile_'+ localtime_str + '.csv'
		print(filename)
		with open(filename,"w") as f:
			df_data.to_csv(f, columns = column_headers) 
		self.filename_signal.emit(filename)
		return filename
		
		
	#----------------------------------- read and implement algorithm from Setup.xls "Algorithm" sheet. ----------------------------
	def algorithm(self):
		# A) Get input and output list.
		
		# A.1) get list of monitored variables as "input_var_list"
		# A set unorderd collection without any duplicates. Note that we have repititions in algorithm page, send set(list)
		# then we sort them ?
		dependent_var_list = list(sorted(set(df_algorithm['Monitored_variables'].tolist())))
		# A.2) Make a list of "Action"/output variables
		action_var_list = [var for var in list(df_algorithm.columns) if (var not in dependent_var_list) and (var != 'Monitored_variables')]
		
		# B) For each index (row) of df_algorithm...
		for idx in	df_algorithm.index:
			# B.1) Drop all columns which are empty. Also, the first column containing the monitored variabls is skipped.
			# NOTE: as per https://www.shanelynn.ie/select-pandas-dataframe-rows-and-columns-using-iloc-loc-and-ix/
			# iloc give series if only one row or one column is selected. To avoid this (since we want column names too= dataframe format)
			# one must pass a "list type" for column indices as below.			
			df_with_contents = df_algorithm.iloc[[idx],1:]
			# drop columns 'in place'
			df_with_contents.dropna(axis=1,inplace=True)
			
			# B.1.1) identify only the input variables as a separate dataframe.
			# ref: https://stackoverflow.com/questions/40636514/selecting-pandas-dataframe-column-by-list
			# https://stackoverflow.com/questions/14940743/selecting-excluding-sets-of-columns-in-pandas
			cols = [col for col in df_with_contents.columns if col not in action_var_list]
			df_with_conditions = df_with_contents[cols]
			# B.1.2) identify only the output variables as a separate dataframe.
			cols = [col for col in df_with_contents.columns if col not in dependent_var_list]
			df_with_actions = df_with_contents[cols]
			
			# B.2) only proceed if there's atleast 1 interdependent variable and 1 action variable.
			# ref: https://stackoverflow.com/questions/19828822/how-to-check-whether-a-pandas-dataframe-is-empty
			if (len(df_with_conditions.columns) >= 1) and (len(df_with_actions.columns) >= 1):			
				# first set a conditions_count variable that keeps tab of conditions met.
				conditions_count = 0
				# record the max conditions to be evaluated in the following code.
				max_conditions = len(df_with_conditions.columns)
				# get the var that is being compared to the rest.
				monitored_var = df_algorithm.iloc[idx,0]
				
				#print("monitored var:", monitored_var)
				
				# B.2.1) Now do condition analysis ------------------------------
				# check for A) within range or B)comparison conditions...
				for any_var in df_with_conditions.columns:
					# code for breaking up stuff if they are a list
					any_var_value = str(df_with_conditions.iloc[0][any_var])
					# B.2.1.A) Find if the any of the input variables have a range parameter (list type [A,B]))
					# check fro all independent variables including the dependent variable.
					if '[' in any_var_value: 
						any_var_value = any_var_value.strip('[]')
						any_var_range = any_var_value.split(',')
						#print(" any_var_range:", any_var_range)
						# special chec for times due to format issues
						if any_var=='Time':
							if self.time_in_range(any_var_range): conditions_count += 1
						else:
							# Regular check for other variabls
							if int(any_var_range[0]) <= globals()[any_var] <= int(any_var_range[1]): conditions_count += 1
					else:
						# B.2.1.B) Find if any input vars have a conditional (>,<,==,!=) paramter wrt input_var
						# Do this only if monitored var is different from the compared (any) var
						for comparison_symbol in ['>','<','=']:
							if comparison_symbol in	 any_var_value:
								print(comparison_symbol)
								if monitored_var != any_var:
									# setup 'y' as the input var's current value
									y = globals()[monitored_var]
									# get the current value of the dependent_var and put in new variable x
									x = globals()[any_var]
									#print(any_var_value)
									# dangereous!!!
									if eval(any_var_value): 
										conditions_count += 1
								break
						
						for boolean_symbol in ['ON','OFF']:
							if boolean_symbol in any_var_value:
								print(boolean_symbol)
								if boolean_symbol=='ON' and globals()[monitored_var]==1:
									conditions_count += 1
								if boolean_symbol=='OFF' and globals()[monitored_var]==0:
									conditions_count += 1
				
				# B.2.2) Finally, if all conditions have been evaluated, the do the action
				if conditions_count == max_conditions:
					# find the actionalble variables.
					for action_var in df_with_actions.columns:
						if action_var != 'Message':
							value=0
							action =  str(df_with_actions.iloc[0][action_var])
							if action.find('ON')	!= -1:	value = 1
							elif action.find('OFF')	!= -1:	value = 0
							print(monitored_var,action_var,globals()[action_var],value)
							if (globals()[action_var] != value):
								self.algorithm_action_signal.emit(action_var,value)
						else:
							message_string = str(df_algorithm.iloc[idx]['Message'])
							self.algorithm_message_signal.emit(message_string)
						
	
	
	
	#-------------------------- Main running loop ------------------------------------------
	def run(self):
		# from Ref: 
		# "The run() method is where we perform the processing that occurs in the thread provided by the Worker instance:"
		#"Note: This is never called directly. It is called by Qt once the
		# thread environment has been set up."	
		
		# Initialize times (unix time)
		# Already set them as previous times for this first time so that data logging starts right at the first go.
		localtime = datetime.now() - timedelta(seconds = 10)
		
		# set the variable for a new log file to default = true.
		self.MW.new_log_file = True
		
		# Init the df_data dataframe status flag.
		new_df_data = True
		data_row_old=[]
		
		# Do this while loop continiously, as long as GUI is running. Except, from ref:
		# "We draw ... as long as the exiting attribute remains False. 
		# This additional check allows us to terminate the thread on demand by setting the exiting attribute to True at any time."
		while not self.exiting:
			if datetime.now() - localtime >= timedelta(seconds = 10):
				
				# Update the local time
				# Note that RTC is directly updating RPi time and is not interacting with this program.
				localtime_old = localtime
				localtime = datetime.now()
			
				# Convert into string for sake of column headers
				localtime_date_str = localtime.strftime('%d/%m/%y')
				localtime_time_str = localtime.strftime('%H:%M:%S')	# eliminating seconds as it makes it easy to compare in algorithm mode.
				print(localtime_time_str)

				# Create a new and empty row for this time stamp
				data_row = [localtime_date_str,localtime_time_str]
				
				# Update data from all cards
				data_row = self.update_IO(data_row)
				
				#print(globals()['Juice_pump'])
				
				#-----------------------------------------------------------------------------
				# Now emit the datarow to the GUI for updating the GUI 'Schematic' page fields.
				# ref: https://www.saltycrane.com/blog/2008/01/pyqt-how-to-pass-arguments-while/
				self.IO_data_ready.emit(data_row)
				
				# if the Algorithm mode is on, then proceed with algo reading, checking and action
				#print('Algo mode: ',globals()['Auto_mode_status'])
				if globals()['Auto_mode_status']== 1: 
					self.algorithm()
				
				# If dataframe is new, then create it with data_row
				if new_df_data == True:
					df_data = pd.DataFrame([data_row],columns = column_headers)
					new_df_data = False
				else:
					# add row to dataframe.
					df_data_new_row = pd.DataFrame([data_row],columns = column_headers)
					df_data = df_data.append(df_data_new_row)	   
				
				
				# write file only if dataframe is heavy. this will limit IO operations
				# which are usually a bottleneck.* (guessing)
				# here it is 30 rows ~ 5 minutes
				if len(df_data.index) >= 3:
					if self.MW.new_log_file == True:
						self.MW.new_log_file = False
						filename = self.make_new_logfile(localtime,df_data)
						log_file_start_time = localtime
					else:
						time_since_log_file_begining = localtime - log_file_start_time
						if	time_since_log_file_begining/timedelta(hours=1) >= 2:
							new_log_file=True
						# append data to logfile.
						# using 'with' syntax it is ensured that the file is closed after end of with statement.
						with open(filename,"a") as f:
						  df_data.to_csv(f, header=False)
					# Set flag to start a new dataframe in the next iteration
					new_df_data = True
			  
				
				# if len(data_row_old) != 0:
					# # Pass latest two rows for plotting	 
					# df_sample = pd.DataFrame([data_row_old,data_row],columns=column_headers)	 
					# self.Plot_data.emit(df_sample)
					# #self.do_plots(df_sample)
				
				# Copy data_row for use in plotting for next iteration
				data_row_old = data_row
								
				#print((datetime.now() - localtime).total_seconds())
				self.sleep(10-(datetime.now() - localtime).total_seconds())


