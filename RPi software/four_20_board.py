#!/usr/bin/env python
#
# Modifications of code from "http://pi.gids.nl:81/adc/"
import global_functions_variables as gfv

#-----------------------------------------------------------------------
# Function to read SPI data from MCP3208 chip, 8 possible adc's (0 thru 7)
# Modifications of code from "http://pi.gids.nl:81/adc/"
#-----------------------------------------------------------------------

class four_20_board(gfv.QObject):
	
	#-----------------------------------------------------------------------
	# Init the board
	#-----------------------------------------------------------------------
	def __init__(self,MainWindow,instance, parent=None):
		gfv.QObject.__init__(self, parent)
		
		self.MW = MainWindow
		self.instance = instance
		
		self.CLK_pin,self.MISO_pin,self.MOSI_pin,self.CS_pin = gfv.get_config_parameters('4_20mA_board', instance,'CLK_pin','MISO_pin','MOSI_pin','CS_pin')
		
		#print(CLK_pin,MISO_pin,MOSI_pin,CS_pin)
		
		# set up the SPI interface pins
		if gfv.mode == 'RPi':
			gfv.GPIO.setup(self.MOSI_pin, gfv.GPIO.OUT)
			gfv.GPIO.setup(self.MISO_pin, gfv.GPIO.IN )
			gfv.GPIO.setup(self.CLK_pin,  gfv.GPIO.OUT)
			gfv.GPIO.setup(self.CS_pin,   gfv.GPIO.OUT)
		print('Initialized four_20_board instance:',self.instance)
	
	def readadc(self, adcnum, clockpin, mosipin, misopin, cspin):
			if ((adcnum > 7) or (adcnum < 0)):
					return -1
			gfv.GPIO.output(cspin   , True)

			gfv.GPIO.output(clockpin, False)  # start clock low
			gfv.GPIO.output(cspin   , False)  # bring CS low

			commandout = adcnum
			commandout |= 0x18            # start bit + single-ended bit
			commandout <<= 3              # we only need to send 5 bits here
			for i in range(5):
					if (commandout & 0x80):
						gfv.GPIO.output(mosipin, True )
					else:
						gfv.GPIO.output(mosipin, False)
					commandout <<= 1
					gfv.GPIO.output(clockpin, True )
					gfv.GPIO.output(clockpin, False)

			adcout = 0

			# read in one empty bit, one null bit and 12 ADC bits
			# pro desetibitovy prevodnik tu bylo puvodne cislo 12
			for i in range(14):
					gfv.GPIO.output(clockpin, True )
					gfv.GPIO.output(clockpin, False)
					adcout <<= 1
					if (gfv.GPIO.input(misopin)):
							adcout |= 0x1

			gfv.GPIO.output(cspin, True)
			
			adcout >>= 1 # first bit is 'null' so drop it
			return adcout




	#-----------------------------------------------------------------------
	# Function to read board, used for updating df.
	#-----------------------------------------------------------------------
	def read_4_20mA_board(self):
		Varray =[0]*8
		for i in range(7):
			# CODE = readadc(i, CLK_pin, MOSI_pin, MISO_pin, CS_pin)
			
			CODE = self.readadc(i, self.CLK_pin, self.MOSI_pin, self.MISO_pin, self.CS_pin)
			
			# NOTE: althrough this chip measures uptill 3.3v, the
			# objective here is to interface 4-20ma through a 500ohm resistor (2-10V)
			# or 0-10 V. Further manipulation should be done as needed.
			Varray[i] = 10.0 * CODE / 4096.0 
			
			gfv.time.sleep(0.01)
		#print(Varray)
		return Varray

