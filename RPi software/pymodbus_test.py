# Check installed USB devices by:
# python -m serial.tools.list_ports -v

# some ref: https://www.tanzolab.it/linuxmodbus

from pymodbus.client.sync import ModbusSerialClient as ModbusClient
from pymodbus.constants import Endian
from pymodbus.payload import BinaryPayloadDecoder

import time


client1 = ModbusClient(method='rtu', port='/dev/ttyUSB0', timeout=5, stopbits = 1, bytesize = 8,  parity='E', baudrate= 9600)
client1.connect()

#client2 = ModbusClient(method='rtu', port='/dev/ttyUSB1', timeout=5, stopbits = 1, bytesize = 8,  parity='E', baudrate= 9600)
#client2.connect()

print(client1.connect())
#print(client2.connect())
# See this page for converting holding register values from power meter to actual register addresses.

#request1 = client1.read_holding_registers(158,2,unit=0x01)
#print (request1.registers)


while(True):
	client1.connect()
	request1 = client1.read_holding_registers(158,2,unit=0x01)
	#decoder1 = BinaryPayloadDecoder.fromRegisters(request1.registers, byteorder=Endian.Big, wordorder=Endian.Little)
	#print ("EM1 Wh.: ",float('{0:.2f}'.format(decoder1.decode_32bit_float())) )
	print ("EM1: ",request1.registers)
	
	#client2.connect()
	#request2 = client2.read_holding_registers(158,2,unit=0x01)
	# ref: https://stackoverflow.com/questions/53010239/pymodbus-read-holding-registers
	#decoder2 = BinaryPayloadDecoder.fromRegisters(request2.registers, byteorder=Endian.Big, wordorder=Endian.Little)
	#print ("EM2 Wh.: ",float('{0:.2f}'.format(decoder2.decode_32bit_float())) )
	#print ("EM2: ",request2.registers)
	time.sleep(5)

#print "Current: %.1f A" % (float(request.registers[1])/10.0)


	