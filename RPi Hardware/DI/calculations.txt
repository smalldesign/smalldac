Cal:
Input to Opto=24V
PC817 CTR@ 5mA = 80-120%. Lets say it is minimum 50%. 
Also forward IRED drop=1.2V
So: (24-1.2)/5mA =4.5K input resistor.

For PC817
(ref: https://www.ecnmag.com/article/2010/12/design-guidelines-transistor-output-optocouplers)
CTRmin = 50%
If ~ 1mA or 5mA? Lets say 5mA.
Then Rf = (24V-1.5V)/5mA = 4500
With CTR eqn: Ic = CTRmin * If * 100 = 2.5mA
If Ic > 2.5mA, then linear mode.
Also, since Vce_sat ~0.2V: The Ic must be such that Vce < Vce_sat. Which is Vce = Vcc-Vl = Vcc - Ic*Rl < 0.2V.
At Ic=1mA, Rl > (Vcc-Vce_sat)/ Ic ~ 4.8k.
As per ref: Keep Rl_actual=2*Rl_calculated to be sure = 10k.
This results in loss of time response of the transistor. So lets keep Rl_calculated*1.3 ~6.2k
What is the saturation current with Rl=6.2k?
- Isat = (Vcc-Vce_sat)/Rl = (5V-0.2V)/6.2k = 0.7 mA which is many times lesser than the CTR computed current available (2.5mA)
This is computed for CTRmin wich is 50%. However, at 5mA, the rated CTR for PC817 is about 100%. So Ic=If*CTR = 5mA.
Since we are well below this the transistor is in saturation mode.

Filter cap
(ref: https://www.digikey.in/en/articles/techzone/2012/apr/protecting-inputs-in-digital-electronics)
Max input frequency= 1kHz
Peak rising time = (1/1kHz) / 100 ~10 uS 
Peak rime response = 2.2*Rf*Cf
Thus: Cf = 10E-6/2.2/4.5k = 1nF