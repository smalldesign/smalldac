Note that pulldowns on all pins ensure that when MCP23017 is not activated, all outputs are zero!

PC817 LED side:
    CASE 1
        Current If = (5V-1.5V)/1k = 3.5mA.
        Total current possible from MCP23017 pins ~25mA and whole (16pins) = 150mA
        so for 1k resistor, 3.5mA*16 = 56mA.
        However, lets take If=5mA.
        Therefore, Rs = (5V-1.5V)/5mA = 700 ~680 ohm.
        Resistor dissipation = 0.005^2 * 680 = 20mW
    CASE 2 
        if Vcc=3.3V instead of 5V, the I2C level shifter can be avoided.
        In this case: assuming 5mA is needed for completly driving the LED:
            (3.3V-1.5V)/5mA = 360 ohm only.
        Total current (16pins) = 16*5mA = 80mA. 
        --- THIS IS NOT GOOD FOR RPi AS MAX POSSIBLE FROM 3.3V PIN IS 50mA

PC817 transistor side
    It does not matter what is Vcc on the LED side. Only If matters.
    CTR nominal is 100% at If = 5mA. Minimum gauranteed CTRmin=50%.
    So, Ic limit = 5mA at CTR=100 or 2.5mA at CTR=50.
    For saturation to work, Ic should be much less than 2.5 to be sure.
    Lets say Ic = 1mA.
    Since transistor side is driven by 24V:
        Vce_sat for ULN2803~1.6V
        minimum current to switch on ULN2803 = 1mA
        (Vsup-Vce_sat)/22k = (24V-1.6V)/1mA ~ 22k.
