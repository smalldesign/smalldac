PCBNEW-LibModule-V1  
# encoding utf-8
Units mm
$INDEX
TO127P254X495-3P
$EndINDEX
$MODULE TO127P254X495-3P
Po 0 0 0 15 00000000 00000000 ~~
Li TO127P254X495-3P
Cd 
Sc 00000000
At STD
Op 0 0 0
.SolderMask 0
.SolderPaste 0
T0 -1.85712 -2.39136 1.64259 1.64259 0 0.05 N V 21 "TO127P254X495-3P"
T1 -1.65369 4.80837 1.64265 1.64265 0 0.05 N V 21 "VAL**"
DA -1.27958 1.27977 1.3208 1.27 -2050 0 21
DA -1.38407 1.26864 1.0668 2.413 -250 0 21
DS -3.3274 2.8448 0.7874 2.8448 0.1524 24
DA -1.27 1.2661 0.7874 2.8448 -2550 0 24
$PAD
Sh "1" C 1.4986 1.4986 0 0 0
Dr 0.9906 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po -2.54 2.54
$EndPAD
$PAD
Sh "2" R 1.4986 1.4986 0 0 0
Dr 0.9906 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po -1.27 0
$EndPAD
$PAD
Sh "3" R 1.4986 1.4986 0 0 0
Dr 0.9906 0 0
At STD N 00C0FFFF
.SolderMask 0
Ne 0 ""
Po 0 2.54
$EndPAD
$EndMODULE TO127P254X495-3P
